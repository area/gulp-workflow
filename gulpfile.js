var gulp   = require('gulp');
var sass   = require('gulp-sass');
var rename = require('gulp-rename');
var base64 = require('gulp-base64');
var minifycss = require('gulp-minify-css');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
/**
 * 使用说明如下：
 * https://git.oschina.net/area/gulp-css-gmspriter
 */
var gm  = require('gulp-gm');
var cssSprite = require('gulp-css-spritesmith');
var runSequence = require('run-sequence');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;

//开发编译目录
var _sources={
    scss_dir:"assets/stylesheet/_sources",
    css_dir:"assets/stylesheet",
    js_dir:"assets/js",
    img_dir:"assets/images/"
};

//打包目录
var _dist={
    css_dir:"dist/stylesheet",
    js_dir:"dist/js",
    img_dir:"dist/images"
};

//雪碧图配置
var _sprite={
    is_mobile:false,
    img_path:"../../images",//替换图片路径匹配
    dest_path:"assets/images/sprites",//输出雪碧图路径
    slice_dir:"assets/images/sprites/slice",//css图片匹配路径
    icons_dir:"assets/images/sprites/icons",//原始图片路径
    css_dir:"./"//css生产目录路径
};

//android
gulp.task('gm-android', function() { 
   return gulp.src(_sprite.icons_dir+"/*.{png,jpg}")
  .pipe(gm(function (gmfile, done) {
    gmfile.size(function (err, size) {
      done(null, gmfile.resize(
        size.width * 0.5,
        size.height * 0.5
      ));
    });
  }))
  .pipe(gulp.dest(_sprite.slice_dir));
});

//ios
gulp.task('gm-ios', function() { 
  return gulp.src(_sprite.icons_dir+"/*.{png,jpg}")
  .pipe(gm(function (gmfile, done) {
    gmfile.size(function (err, size) {
      done(null, gmfile.resize(
        size.width * 1,
        size.height * 1
      ));
    });
  }))
  .pipe(rename(function (path) {
        path.basename += "@2x";
   }))
  .pipe(gulp.dest(_sprite.slice_dir));
});

//注意：如果是移动端预先执行2x图片的编译，pc端直接将原始图放到slice目录即可
gulp.task('gmMobile',['gm-android','gm-ios']);

gulp.task('sprite',["sass"], function() {
   return gulp.src(_sources.css_dir+"/**/*.css").pipe(cssSprite({
        // sprite背景图源文件夹，只有匹配此路径才会处理，默认 images/slice/
        imagepath: _sprite.slice_dir,
        // 映射CSS中背景路径，支持函数和数组，默认为 null
        imagepath_map: null,
        // 雪碧图输出目录，注意，会覆盖之前文件！默认 images/
        spritedest:_sprite.dest_path,
        // 替换后的背景路径，默认 ../images/
        spritepath: _sprite.img_path,
        // 各图片间间距，如果设置为奇数，会强制+1以保证生成的2x图片为偶数宽高，默认 0
        padding: 20,
        // 是否使用 image-set 作为2x图片实现，默认不使用
        useimageset: false,
        // 是否以时间戳为文件名生成新的雪碧图文件，如果启用请注意清理之前生成的文件，默认不生成新文件
        newsprite: false,
        // 给雪碧图追加时间戳，默认不追加
        spritestamp: true,
        // 默认使用二叉树最优排列算法
        // algorithm: 'top-down',
        // 在CSS文件末尾追加时间戳，默认不追加
        cssstamp: false
    }))
    .pipe(gulp.dest(_sprite.css_dir));
});


gulp.task('sass', function() {
    return  gulp.src(_sources.scss_dir+"/**/*.scss")
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(sourcemaps.write("./_sourcemaps"))
        .pipe(gulp.dest(_sources.css_dir));
});


gulp.task("cssbase",function(){
    return gulp.src(_sources.css_dir+"/**/*.css")
        .pipe(base64({
            extensions: ['svg', 'png'],
            //过滤不需要的路径
            exclude:[/\/images\/standard/],
            deleteAfterEncoding: false
        }))
        .pipe(gulp.dest(_sources.css_dir));
});

gulp.task( 'minImg', function(){
    return gulp.src( _sources.img_dir+"/**/*.{png,jpg,gif,bmp}")
       .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
       .pipe(gulp.dest(_dist.img_dir));
});

gulp.task('minCss', function() { 
    return    gulp.src(_sources.css_dir+'/**/*.css')
        .pipe(autoprefixer('last 2 version', 'safari 5',  'ie 8', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest(_dist.css_dir));
});

gulp.task('minJs', function() {  
  return  gulp.src(_sources.js_dir+'/**/*.js')
    .pipe(rename({suffix: '.min'}))
    .pipe(jshint())
    .pipe(uglify())
    .pipe(gulp.dest(_dist.js_dir));
});

gulp.task('server', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("html/**/*.html").on('change', reload);
});

gulp.task('watch',function(){
    gulp.watch(_sources.scss_dir+"/**/*.scss",['sass']);
});   

gulp.task('default',function(){
    gulp.start('watch');
});

gulp.task("package", function(){
    runSequence(
        'sass',
        'sprite',
        'minCss',
        'minJs',
        'minImg'
      )
});
